package br.com.sicredi.simulacao.controller;

import java.net.URI;
import java.text.MessageFormat;
import java.util.Optional;
import java.util.function.Consumer;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.sicredi.simulacao.dto.MessageDTO;
import br.com.sicredi.simulacao.dto.SimulacaoDTO;
import br.com.sicredi.simulacao.dto.ValidacaoDTO;
import br.com.sicredi.simulacao.entity.Simulacao;
import br.com.sicredi.simulacao.exception.SimulacaoException;
import br.com.sicredi.simulacao.repository.SimulacaoRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "/simulacoes", tags = "Simulações")
public class SimulacaoController {

	private final SimulacaoRepository repository;

	public SimulacaoController(SimulacaoRepository repository) {
		this.repository = repository;
	}

	@GetMapping("/api/v1/simulacoes")
	@ApiOperation(value = "Retorna todas as simulações existentes")
	@ApiResponses({
			@ApiResponse(code = 200, message = "Simulações encontradas", response = SimulacaoDTO.class, responseContainer = "List"),
			@ApiResponse(code = 204, message = "Não existem Simulações", response = Void.class) })
	ResponseEntity<?> getSimulacao() {
		if (repository.findAll().isEmpty() || repository.findAll().equals(null)) {
			return ResponseEntity.noContent().build();
		}
		return ResponseEntity.ok().body(repository.findAll());
	}

	@GetMapping("/api/v1/simulacoes/{cpf}")
	@ApiOperation(value = "Retorna uma simulação através do CPF")
	@ApiResponses({ @ApiResponse(code = 200, message = "Simulação encontrada", response = SimulacaoDTO.class),
			@ApiResponse(code = 404, message = "Simulação não encontrada", response = MessageDTO.class) })
	ResponseEntity<SimulacaoDTO> one(@PathVariable String cpf) {
		return repository.findByCpf(cpf)
				.map(simulacao -> ResponseEntity.ok().body(new ModelMapper().map(simulacao, SimulacaoDTO.class)))
				.orElseThrow(() -> new SimulacaoException(MessageFormat.format("CPF {0} não encontrado", cpf)));
	}

	@PostMapping("/api/v1/simulacoes")
	@ApiOperation(value = "Insere uma nova simulação", code = 201)
	@ApiResponses({ @ApiResponse(code = 201, message = "Simulação criada com sucesso", response = SimulacaoDTO.class),
			@ApiResponse(code = 400, message = "Falta de informações", response = ValidacaoDTO.class),
			@ApiResponse(code = 409, message = "CPF já existente") })
	ResponseEntity<?> novaSimulacao(@Valid @RequestBody SimulacaoDTO simulacao, UriComponentsBuilder uriBuilder) {

		if (repository.findByCpf(simulacao.getCpf()).isPresent()) {
			return new ResponseEntity<>("CPF já existente", HttpStatus.CONFLICT);
		}
		;
		
		URI uri = uriBuilder.path("/simulacoes/{cpf}").buildAndExpand(simulacao.getCpf()).toUri();
		return ResponseEntity.created(uri).body(repository.save(new ModelMapper().map(simulacao, Simulacao.class)));
	}

	@PutMapping("/api/v1/simulacoes/{cpf}")
	@ApiOperation(value = "Atualiza uma simulação existente através do CPF")
	@ApiResponses({ @ApiResponse(code = 200, message = "Simulação alterada com sucesso", response = SimulacaoDTO.class),
			@ApiResponse(code = 404, message = "Simulação não encontrada", response = MessageDTO.class),
			@ApiResponse(code = 409, message = "CPF já existente") })
	Simulacao atualizaSimulacao(@RequestBody SimulacaoDTO simulacao, @PathVariable String cpf) {

		return update(new ModelMapper().map(simulacao, Simulacao.class), cpf)
				.orElseThrow(() -> new SimulacaoException(MessageFormat.format("CPF {0} não encontrado", cpf)));
	}

	@DeleteMapping("/api/v1/simulacoes/{id}")
	@ApiOperation(value = "Remove uma simulação existente através do CPF")
	@ApiResponses({ @ApiResponse(code = 200, message = "Simulação removida com sucesso") })
	ResponseEntity<String> delete(@PathVariable Long id) {
		if (repository.findById(id).isPresent()) {
			repository.deleteById(id);
			return new ResponseEntity<>("OK", HttpStatus.NO_CONTENT);
		}
		;

		return new ResponseEntity<>("Simulação não encontrada", HttpStatus.NOT_FOUND);
	}

	private Optional<Simulacao> update(Simulacao novaSimulacao, String cpf) {
		return repository.findByCpf(cpf).map(simulacao -> {
			setIfNotNull(simulacao::setId, novaSimulacao.getId());
			setIfNotNull(simulacao::setNome, novaSimulacao.getNome());
			setIfNotNull(simulacao::setCpf, novaSimulacao.getCpf());
			setIfNotNull(simulacao::setEmail, novaSimulacao.getEmail());
			setIfNotNull(simulacao::setValor, novaSimulacao.getValor());
			setIfNotNull(simulacao::setParcelas, novaSimulacao.getParcelas());
			setIfNotNull(simulacao::setSeguro, novaSimulacao.getSeguro());

			return repository.save(simulacao);
		});
	}

	private <T> void setIfNotNull(final Consumer<T> setter, final T value) {
		if (value != null) {
			setter.accept(value);
		}
	}

}
