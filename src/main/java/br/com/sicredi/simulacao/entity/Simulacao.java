package br.com.sicredi.simulacao.entity;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.*;

import br.com.sicredi.simulacao.builder.SimulacaoBuilder;

import java.math.BigDecimal;

@Data
@Builder(access = AccessLevel.PUBLIC)
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(uniqueConstraints = {
        @UniqueConstraint(name = "cpf_unique", columnNames = "cpf")
})
public class Simulacao {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull(message = "Nome não pode ser vazio")
    private String nome;

    @NotNull(message = "CPF não pode ser vazio")
    @Size(min=11,max=11,message="CPF deve ter no máximo {max} caracteres, apenas números")
    private String cpf;

    @NotNull(message = "E-mail não deve ser vazio")
    @Email
    @Pattern(regexp = ".+@.+\\.[a-z]+", message = "E-mail deve ser um e-mail válido")
    private String email;

    @NotNull(message = "Valor não pode ser vazio")
    @Min(value = 1000, message = "Valor deve ser maior ou igual a R$ 1.000 e menor ou igual a R$ 40.000")
    @Max(value = 40000, message = "Valor deve ser maior ou igual a R$ 1.000 e menor ou igual a R$ 40.000")
    private BigDecimal valor;

    @NotNull(message = "Parcelas não pode ser vazio")
    @Min(value = 2, message = "Parcelas deve ser igual ou maior que 2 e menor ou igual a 48")
    @Max(value = 48, message = "Parcelas deve ser igual ou maior que 2 e menor ou igual a 48")
    private Integer parcelas;

    @NotNull(message = "Uma das opções de Seguro devem ser selecionadas")
    private Boolean seguro;

	public Long getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public String getCpf() {
		return cpf;
	}

	public String getEmail() {
		return email;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public Integer getParcelas() {
		return parcelas;
	}

	public Boolean getSeguro() {
		return seguro;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public void setParcelas(Integer parcelas) {
		this.parcelas = parcelas;
	}

	public void setSeguro(Boolean seguro) {
		this.seguro = seguro;
	}
	

	public static SimulacaoBuilder builder() {
		return new SimulacaoBuilder();
	}
	
	

    
    
}
