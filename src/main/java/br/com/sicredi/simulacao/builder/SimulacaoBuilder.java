package br.com.sicredi.simulacao.builder;

import java.math.BigDecimal;

import br.com.sicredi.simulacao.entity.Simulacao;

public class SimulacaoBuilder {
	private Long id;
	private String nome;
	private String cpf;
	private String email;
	private BigDecimal valor;
	private Integer parcelas;
	private Boolean seguro;


	public SimulacaoBuilder cpf(String cpf) {
		this.cpf = cpf;
		return this;
	}
	
	public SimulacaoBuilder nome(String nome) {
		this.nome = nome;
		return this;
	}

	public SimulacaoBuilder email(String email) {
		this.email = email;
		return this;
	}
	
	public SimulacaoBuilder valor(BigDecimal valor) {
		this.valor = valor;
		return this;
	}
	
	public SimulacaoBuilder parcelas(Integer parcelas) {
		this.parcelas = parcelas;
		return this;
	}
	
	public SimulacaoBuilder seguro(Boolean seguro) {
		this.seguro = seguro;
		return this;
	}
	
	public SimulacaoBuilder id(Long id) {
		this.id = id;
		return this;
	}
	
	public Simulacao build() {
		Simulacao simulacao = new Simulacao();
		simulacao.setId(id);
		simulacao.setCpf(cpf);
		simulacao.setNome(nome);
		simulacao.setEmail(email);
		simulacao.setValor(valor);
		simulacao.setParcelas(parcelas);
		simulacao.setSeguro(seguro);
		return simulacao;
	}

}
